package com.momodding.multiiface.controller

import com.momodding.multiiface.service.AliyunSmsService
import com.momodding.multiiface.service.ISmsService
import com.momodding.multiiface.service.InfobipSmsService
import com.momodding.multiiface.service.TelkomselSmsService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Spy
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
internal class SmsControllerTest {
    @Mock
    private lateinit var telkomselSmsService: ISmsService

    @Mock
    private lateinit var infobipSmsService: ISmsService

    @Mock
    private lateinit var aliyunSmsService: ISmsService

    @InjectMocks
    private lateinit var smsController: SmsController

    @Spy
    private var smsService: MutableMap<String, ISmsService> = mutableMapOf()

    @BeforeEach
    fun setup() {
        smsService[TelkomselSmsService.TELKOMSEL_SMS_SERVICE] = telkomselSmsService
        smsService[InfobipSmsService.INFOBIP_SMS_SERVICE] = infobipSmsService
        smsService[AliyunSmsService.ALIYUN_SMS_SERVICE] = aliyunSmsService
    }

    @Test
    fun `success send with telkomsel`() {
        Mockito.`when`(telkomselSmsService.sendSms()).thenReturn("telkomsel")

        val result = smsController.sendSms()
        Assertions.assertEquals("telkomsel", result.body as String)
    }
}
