package com.momodding.multiiface

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MultiIfaceApplication

fun main(args: Array<String>) {
    runApplication<MultiIfaceApplication>(*args)
}
