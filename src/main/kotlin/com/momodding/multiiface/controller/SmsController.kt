package com.momodding.multiiface.controller

import com.momodding.multiiface.service.AliyunSmsService
import com.momodding.multiiface.service.ISmsService
import com.momodding.multiiface.service.InfobipSmsService
import com.momodding.multiiface.service.TelkomselSmsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["sms"], produces = [MediaType.APPLICATION_JSON_VALUE])
class SmsController @Autowired constructor(
    private val smsService: Map<String, ISmsService>
) {

    @GetMapping("send")
    fun sendSms(): ResponseEntity<Any> {
        val listOfBean = listOf(
            TelkomselSmsService.TELKOMSEL_SMS_SERVICE,
            InfobipSmsService.INFOBIP_SMS_SERVICE,
            AliyunSmsService.ALIYUN_SMS_SERVICE
        )

        val result = run breaking@ {
            listOfBean.map {
                val sms = try {
                    smsService[it]?.sendSms()
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    return@map
                }
                return@breaking sms
            }
        }
        return ResponseEntity(result, HttpStatus.OK)
    }
}
