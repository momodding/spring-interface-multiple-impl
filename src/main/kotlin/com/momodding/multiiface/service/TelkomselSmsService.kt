package com.momodding.multiiface.service

import org.springframework.stereotype.Service

@Service(TelkomselSmsService.TELKOMSEL_SMS_SERVICE)
class TelkomselSmsService: ISmsService {
    companion object {
        const val TELKOMSEL_SMS_SERVICE = "TelkomselSmsService"
    }
    override fun sendSms(): String {
        throw Exception("failed send sms")
    }
}
