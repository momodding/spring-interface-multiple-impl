package com.momodding.multiiface.service

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Service

@Service(AliyunSmsService.ALIYUN_SMS_SERVICE)
class AliyunSmsService: ISmsService {
    companion object {
        const val ALIYUN_SMS_SERVICE = "AliyunSmsService"
    }

    override fun sendSms(): String {
        return "send sms from aliyun"
    }
}
