package com.momodding.multiiface.service

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Service

@Service(InfobipSmsService.INFOBIP_SMS_SERVICE)
class InfobipSmsService: ISmsService {
    companion object {
        const val INFOBIP_SMS_SERVICE = "InfobipSmsService"
    }
    override fun sendSms(): String {
        return "send sms from infobip"
    }
}
