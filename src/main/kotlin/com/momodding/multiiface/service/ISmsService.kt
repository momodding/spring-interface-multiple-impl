package com.momodding.multiiface.service

interface ISmsService {
    fun sendSms(): String
}
